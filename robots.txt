User-agent: *

Sitemap: https://www.vietnammuslimtourkaka.com/sitemap.xml

Disallow: /assets/
Disallow: /doc/
Disallow: /install/
Disallow: /lib/
Disallow: /modules/
Disallow: /module_custom/
Disallow: /plugins/
Disallow: /scripts/
Disallow: /tmp/

Allow: /assets/sitemaps/
Allow: /assets/themes/
Allow: /tmp/cache/