<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Products;

use \CMSMS\Async\Job;
use CMSMS\Async\CronJob;

/**
 * Description of class
 *
 * @author Lipit
 */
class Job001 extends CronJob {

    private $_age;

    public function __construct() {
        parent::__construct();
        $this->module = 'CmsJobManager';
        $this->frequency = self::RECUR_15M;
        $this->until = strtotime('+1 day');
    }

    public function setAge($age) {
        $this->_age = $age;
    }

    public function getAge() {
        return $this->_age;
    }

    public function execute() {
        // simple test, creates an audit string
        //some_unknown_function(); // intentionally generate an error.
        audit('', 'Products', 'Job001 Test Complete Age: ' . $this->getAge());
        debug_to_log('Job001 Test Complete');
    }

}
