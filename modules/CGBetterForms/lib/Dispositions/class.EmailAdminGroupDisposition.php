<?php
namespace CGBetterForms\Dispositions;
use \CGBetterForms\utils;

class EmailAdminGroupDisposition extends Disposition
{
    private $_group;
    private $_subjecttemplate;
    private $_bodytemplate;

    public function __get($key)
    {
        switch( $key ) {
        case 'group':
            return $this->_group;
        case 'subjecttemplate':
            return trim($this->_subjecttemplate);
        case 'bodytemplate':
            return trim($this->_bodytemplate);
        }
    }

    public function set_group( $gid )
    {
        $this->_group = (int) $gid;
    }

    public function get_group()
    {
        return $this->_group;
    }

    public function set_subjecttemplate( $data )
    {
        $this->_subjecttemplate = trim($data);
    }

    public function set_bodytemplate( $data )
    {
        $this->_bodytemplate = trim($data);
    }

    public function dispose( \CGBetterForms\Form $form, \CGBetterForms\FormResponse& $data)
    {
        $list = null;
        if( $this->_group > 0 ) $list = \cge_userops::expand_group_emails( $this->_group );
        if( !$list ) throw new \RuntimeException('No valid email addresses to send to for EmailAdminGroupDisposition');

        $mailer = \CGBetterForms\utils::get_mailer( $form );
        $added = false;
        foreach( $list as $addr ) {
            $addr = trim($addr);
            if( !$addr ) continue;
            if( !is_email( $addr) ) continue;
            $mailer->AddAddress( $addr );
            $added = true;
        }
        if( !$added ) throw new \RuntimeException('No valid email addresses to send to for EmailAdminGroupDisposition');

        $subject = trim(utils::process_template( $this->subjecttemplate, $form, $data));
        $body = trim(utils::process_template( $this->bodytemplate, $form, $data));
        if( !$subject || !$body ) return;

        $mailer->SetSubject( $subject );
        $mailer->SetBody( $body );
        if( $data->replyto_address && is_email($data->replyto_address) ) {
            $mailer->clearReplyTos();
            $mailer->AddReplyTo( $data->replyto_address );
        }
        $mailer->Send();
    }
} // end of class
