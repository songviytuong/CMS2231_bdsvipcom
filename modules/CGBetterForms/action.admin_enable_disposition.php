<?php
namespace CGBetterForms;
if( !isset($gCms) ) exit;
if( !$this->VisibleToAdminUser() ) exit;
$this->SetCurrentTab('handlers');

try {
    $state       = \cge_param::get_bool($params,'state',1);
    $form_guid   = \cge_param::get_string($params,'form_guid');
    $disp_guid   = \cge_param::get_string($params,'disp_guid');

    $form = $this->get_form( $form_guid );
    $disp = $form->getDisposition( $disp_guid );
    $disp->set_active( $state );
    $form->setDisposition( $disp );
    $form_guid = \CGBetterForms\utils::store_object( $form, $form_guid );
    $disp_guid = \CGBetterForms\utils::store_object( $disp, $disp_guid );
    $form_guid = $this->store_form( $form );
    $this->Redirect('m1_','admin_edit_form', '', [ 'form_guid'=>$form_guid, 'disp_guid'=>$disp_guid, 'cg_activetab'=>'handlers' ] );
}
catch( \Exception $e ) {
    debug_display($e);
    die();
}