<?php

$CMS_ADMIN_PAGE = 1;

require_once("../lib/include.php");
require_once("../lib/classes/class.bookmark.inc.php");
$urlext = '?' . CMS_SECURE_PARAM_NAME . '=' . $_SESSION[CMS_USER_KEY];

check_login();

$bookmark_id = -1;
if (isset($_GET["bookmark_id"])) {
	$bookmark_id = $_GET["bookmark_id"];

	$result = false;

	$bookops = cmsms()->GetBookmarkOperations();
	$markobj = $bookops->LoadBookmarkByID($bookmark_id);

	if ($markobj) {
		$result = $markobj->Delete();
	}
}
redirect("listbookmarks.php" . $urlext);
