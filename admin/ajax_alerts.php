<?php

$CMS_ADMIN_PAGE = 1;
require_once("../lib/include.php");
$urlext = '?' . CMS_SECURE_PARAM_NAME . '=' . $_SESSION[CMS_USER_KEY];
try {
    $out = null;
    $uid = get_userid(FALSE);
    if (!$uid) throw new \Exception('Permission Denied'); // should be a 403, but meh.

    $op = cleanValue($_POST['op']);
    if (!$op) $op = 'delete';
    $alert_name = cleanValue($_POST['alert']);

    switch ($op) {
        case 'delete':
            $alert = \CMSMS\AdminAlerts\Alert::load_by_name($alert_name);
            $alert->delete();
            break;
        default:
            throw new \Exception('Unknown operation ' . $op);
    }
    echo $out;
} catch (\Exception $e) {
    // do 500 error.
    $handlers = ob_list_handlers();
    for ($cnt = 0; $cnt < sizeof($handlers); $cnt++) {
        ob_end_clean();
    }

    header("HTTP/1.0 500 " . $e->GetMessage());
    header("Status: 500 Server Error");
    echo $e->GetMessage();
}
exit;
