<?php

global $CMS_ADMIN_PAGE;
$CMS_ADMIN_PAGE = 1;

require_once('../lib/include.php');
$urlext = '?' . CMS_SECURE_PARAM_NAME . '=' . $_SESSION[CMS_USER_KEY];

include_once("header.php");

check_login();
$config = cmsms()->GetConfig();
$link = $_SERVER['HTTP_REFERER'];
$newmark = new Bookmark();
$newmark->user_id = get_userid();
$newmark->url = $link;
$newmark->title = $_GET['title'];
$result = $newmark->save();

if ($result) {
	header('HTTP_REFERER: ' . $config['admin_url'] . '/index.php');
	redirect($link);
} else {
	include_once("header.php");
	echo "<h3>" . lang('erroraddingbookmark') . "</h3>";
}
