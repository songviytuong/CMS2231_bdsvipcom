<?php

function smarty_function_site_mapper($params, &$smarty)
{
	$params['module'] = 'Navigator';
	
	if( !isset($params['template']) )
    {
		$params['template'] = 'minimal_menu.tpl';
    }
	
	return cms_module_plugin($params,$smarty);
}
