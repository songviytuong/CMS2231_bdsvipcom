<?php

function smarty_function_created_date($params, &$smarty)
{
	$content_obj = CmsApp::get_instance()->get_content_object();

	$format = "%x %X";
	if (!empty($params['format'])) $format = $params['format'];

	if (is_object($content_obj) && $content_obj->GetCreationDate() > -1) {
		$time = $content_obj->GetCreationDate();
		$str = cms_htmlentities(strftime($format, $time));;

		if (isset($params['assign'])) {
			$smarty->assign(trim($params['assign']), $str);
			return;
		}
		return $str;
	}
}

function smarty_cms_about_function_created_date()
{
	?>
	<p>Author: Ted Kulp&lt;tedkulp@users.sf.net&gt;</p>

	<p>Change History:</p>
	<ul>
		<li>None</li>
	</ul>
<?php
}
?>