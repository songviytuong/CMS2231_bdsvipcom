<?php

function smarty_function_image($params, &$smarty)
{
    $gCms = CmsApp::get_instance();
    $text = '';
    $imgstart = '<img src=';
    $imgend = '/>';
    if (!empty($params['src'])) {
        $text = $imgstart .= '"' . $gCms->config['image_uploads_url'] . '/' . $params['src'] . '"';
        $size = @getimagesize($gCms->config['image_uploads_path'] . '/' . $params['src']);

        if (!empty($params['width'])) {
            $text .= ' width="' . $params['width'] . '"';
        } elseif ($size[0] > 0) {
            $text .= ' width="' . $size[0] . '"';
        }

        if (!empty($params['height'])) {
            $text .= ' height="' . $params['height'] . '"';
        } elseif ($size[1] > 0) {
            $text .= ' height="' . $size[1] . '"';
        }

        if (!empty($params['alt'])) {
            $alt = $params['alt'];
        } else {
            $alt = '[' . $params['src'] . ']';
        }

        $text .= ' alt="' . $alt . '"';
        if (!empty($params['title'])) {
            $text .= ' title="' . $params['title'] . '"';
        } else {
            $text .= ' title="' . $alt . '"';
        }

        if (!empty($params['class']))    $text .= ' class="' . $params['class'] . '"';
        if (!empty($params['addtext'])) $text .= ' ' . $params['addtext'];
        $text .= $imgend;
    } else {
        $text = '<!-- empty results from image plugin -->';
    }

    if (isset($params['assign'])) {
        $smarty->assign(trim($params['assign']), $text);
        return;
    }
    return $text;
}


function smarty_cms_about_function_image()
{
    ?>
    <p>Author: Robert Campbell &lt;calguy1000@hotmail.com&gt;,</p>

    <p>Change History</p>
    <ul>
        <li>Initial release</li>
        <li>Added alt param and removed the &lt;/img&gt;</li>
        <li>Added default width, height and alt <small>(contributed by Walter Wlodarski)</small></li>
    </ul>
<?php
}
?>