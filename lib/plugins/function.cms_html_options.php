<?php

function smarty_function_cms_html_options($params, &$smarty)
{
    $options = null;
    if (!isset($params['options'])) {
        if (isset($params['value']) && isset($params['label'])) {
            $opt = array();
            $opt['label'] = $params['label'];
            $opt['value'] = $params['value'];
            if (isset($params['title'])) $opt['title'] = $params['title'];
            if (isset($params['class'])) $opt['class'] = $params['class'];
            $options = $opt;
        } else {
            return;
        }
    } else {
        $options = $params['options'];
    }

    $out = null;
    if (is_array($options) && count($options)) {
        $selected = null;
        if (isset($params['selected'])) {
            $selected = $params['selected'];
            if (!is_array($selected)) $selected = explode(',', $selected);
        }
        $out = CmsFormUtils::create_option($params['options'], $selected);
    }

    if (isset($params['assign'])) {
        $smarty->assign($params['assign'], $out);
        return;
    }
    return $out;
}
