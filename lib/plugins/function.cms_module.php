<?php

function smarty_function_cms_module($params, &$smarty)
{
	return cms_module_plugin($params, $smarty);
}

function smarty_cms_about_function_cms_module()
{
	?>
	<p>Author: Ted Kulp&lt;tedkulp@users.sf.net&gt;</p>

	<p>Change History:</p>
	<ul>
		<li>None</li>
	</ul>
<?php
}
?>