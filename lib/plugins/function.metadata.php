<?php

function smarty_function_metadata($params, &$smarty)
{
	$gCms = CmsApp::get_instance();
	$config = \cms_config::get_instance();
	$content_obj = $gCms->get_content_object();

	$result = '';
	$showbase = true;

	// Show a base tag unless showbase is false in config.php
	// It really can't hinder, only help
	if (isset($config['showbase']))  $showbase = $config['showbase'];

	// But allow a parameter to override it
	if (isset($params['showbase'])) {
		if ($params['showbase'] == 'false')	$showbase = false;
	}

	\CMSMS\HookManager::do_hook('metadata_prerender', ['content_id' => $content_obj->Id(), 'showbase' => &$showbase, 'html' => &$result]);

	if ($showbase) {
		$base = CMS_ROOT_URL;
		if ($gCms->is_https_request()) $base = $config['ssl_url'];
		$result .= "\n<base href=\"" . $base . "/\" />\n";
	}

	$result .= get_site_preference('metadata', '');

	if (is_object($content_obj) && $content_obj->Metadata() != '') $result .= "\n" . $content_obj->Metadata();

	if ((!strpos($result, $smarty->left_delimiter) === false) and (!strpos($result, $smarty->right_delimiter) === false)) {
		$result = $smarty->fetch('string:' . $result);
	}

	\CMSMS\HookManager::do_hook('metadata_postrender', ['content_id' => $content_obj->Id(), 'html' => &$result]);
	if (isset($params['assign'])) {
		$smarty->assign(trim($params['assign']), $result);
		return;
	}
	return $result;
}

function smarty_cms_about_function_metadata()
{
	?>
	<p>Author: Ted Kulp&lt;ted@cmsmadesimple.org&gt;</p>

	<p>Change History:</p>
	<ul>
		<li>None</li>
	</ul>
<?php
}
?>