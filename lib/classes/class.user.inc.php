<?php

class User
{

	/**
	 * @var int $id User id
	 */
	var $id;

	/**
	 * @var string Username
	 */
	var $username;

	/**
	 * @var string $password Password (md5 encoded)
	 */
	var $password;

	/**
	 * @var string $firstname Users First Name
	 */
	var $firstname;

	/**
	 * @var string $lastname Last Name
	 */
	var $lastname;

	/**
	 * @var string $email Users Email Address
	 */
	var $email;

	/**
	 * @var bool $active Active Flag
	 */
	var $active;

	/**
	 * @var bool $adminaccess Flag to tell whether user can login to admin panel
	 */
	var $adminaccess;

	/**
	 * Generic constructor.  Runs the SetInitialValues fuction.
	 */
	function __construct()
	{
		$this->SetInitialValues();
	}

	/**
	 * Sets object to some sane initial values
	 *
	 * @since 0.6.1
	 */
	function SetInitialValues()
	{
		$this->id = -1;
		$this->username = '';
		$this->password = '';
		$this->firstname = '';
		$this->lastname = '';
		$this->email = '';
		$this->active = false;
		$this->adminaccess = false;
	}

	/**
	 * Encrypts and sets password for the User
	 *
	 * @since 0.6.1
	 * @param string $password The plaintext password.
	 */
	function SetPassword($password)
	{
		$this->password = md5(get_site_preference('sitemask','').$password);
	}

	/**
	 * Saves the user to the database.  If no user_id is set, then a new record
	 * is created.  If the uset_id is set, then the record is updated to all values
	 * in the User object.
	 *
	 * @returns mixed If successful, true.  If it fails, false.
	 * @since 0.6.1
	 */
	function Save()
	{
		$result = false;

        $userops = UserOperations::get_instance();
		if ($this->id > -1) {
			$result = $userops->UpdateUser($this);
		}
		else {
			$newid = $userops->InsertUser($this);
			if ($newid > -1) {
				$this->id = $newid;
				$result = true;
			}
		}

		return $result;
	}

	/**
	 * Delete the record for this user from the database and resets
	 * all values to their initial values.
	 *
	 * @returns mixed If successful, true.  If it fails, false.
	 * @since 0.6.1
	 */
	function Delete()
	{
		$result = false;
		if ($this->id > -1) {
			$userops = UserOperations::get_instance();
			$result = $userops->DeleteUserByID($this->id);
			if ($result) $this->SetInitialValues();
		}
		return $result;
	}
}
