<?php

use \CMSMS\HookManager;

class CmsDomainCollection {

    /**
     * @ignore
     */
    const TABLENAME = 'module_multidomains';

    /**
     * @ignore
     */
    private $_data = array();

    /**
     * @ignore
     */
    private $_dirty;

    /**
     * @ignore
     */
    private static $_raw_cache;

    /**
     * @ignore
     */
    private static $_dflt_id;

    /**
     * Get the theme id
     * Only themes that have been saved to the database have an id.
     * @return int
     */
    public function get_id() {
        if (isset($this->_data['id']))
            return $this->_data['id'];
    }

    /**
     * Get the theme name
     * @return string
     */
    public function get_domain() {
        if (isset($this->_data['domain']))
            return $this->_data['domain'];
    }

    /**
     * Set the theme name
     * This marks the theme as dirty
     *
     * @throws CmsInvalidDataException
     * @param string $str
     */
    public function set_domain($str) {
//        if( !CmsAdminUtils::is_valid_itemname($str)) throw new CmsInvalidDataException("Invalid characters in name: $str");
        $this->_data['domain'] = $str;
        $this->_dirty = TRUE;
    }

    /**
     * Get the default flag
     * Note, only one theme can be the default.
     *
     * @return bool
     */
    public function get_default() {
        if (isset($this->_data['dflt']))
            return $this->_data['dflt'];
    }

    /**
     * Sets this theme as the default theme.
     * Sets the dirty flag.
     * Note, only one theme can be the default.
     *
     * @param bool $flag
     */
    public function set_default($flag) {
        $flag = (bool) $flag;
        $this->_data['dflt'] = $flag;
        $this->_dirty = TRUE;
    }

    /**
     * Get the theme description
     *
     * @return string
     */
    public function get_notes() {
        if (isset($this->_data['notes']))
            return $this->_data['notes'];
    }

    /**
     * Set the theme description
     *
     * @param string $str
     */
    public function set_notes($str) {
        $str = trim($str);
        $this->_data['description'] = $str;
        $this->_dirty = TRUE;
    }

    /**
     * Get the creation date of this theme
     * The creation date is specified automatically on the first save
     *
     * @return int
     */
    public function get_created() {
        if (isset($this->_data['created_date']))
            return $this->_data['created_date'];
    }

    /**
     * Get the date of the last modification of this theme
     *
     * @return int
     */
    public function get_modified() {
        if (isset($this->_data['modified_date']))
            return $this->_data['modified_date'];
    }

    /**
     * Validate this object before saving.
     *
     * @throws CmsInvalidDataException
     */
    protected function validate() {
        if ($this->get_domain() == '')
            throw new CmsInvalidDataException('A Domain must have a name');
        if (!CmsAdminUtils::is_valid_itemname($this->get_domain())) {
            throw new CmsInvalidDataException('There are invalid characters in the design name.');
        }
        $db = CmsApp::get_instance()->GetDb();
        $tmp = null;
        if ($this->get_id()) {
            $query = 'SELECT id FROM ' . CMS_DB_PREFIX . self::TABLENAME . ' WHERE name = ? AND id != ?';
            $tmp = $db->GetOne($query, array($this->get_domain(), $this->get_id()));
        } else {
            $query = 'SELECT id FROM ' . CMS_DB_PREFIX . self::TABLENAME . ' WHERE name = ?';
            $tmp = $db->GetOne($query, array($this->get_name()));
        }
        if ($tmp)
            throw new CmsInvalidDataException('Domain with the same name already exists.');
    }

    /**
     * @ignore
     */
    private function _update() {
        if (!$this->_dirty)
            return;
        $this->validate();

        $db = CmsApp::get_instance()->GetDb();
        $query = 'UPDATE ' . CMS_DB_PREFIX . self::TABLENAME . ' SET domain = ?, notes = ?, dflt = ?, modified_date = ? WHERE id = ?';
        $dbr = $db->Execute($query, array($this->get_domain(), $this->get_notes(), ($this->get_default()) ? 1 : 0, time(), $this->get_id()));
        if (!$dbr)
            throw new CmsSQLErrorException($db->sql . ' -- ' . $db->ErrorMsg());

        if ($this->get_default()) {
            $query = 'UPDATE ' . CMS_DB_PREFIX . self::TABLENAME . ' SET dflt = 0 WHERE id != ?';
            $dbr = $db->Execute($query, array($this->get_id()));
            if (!$dbr)
                throw new CmsSQLErrorException($db->sql . ' -- ' . $db->ErrorMsg());
        }

        $this->_dirty = FALSE;
        audit($this->get_id(), 'CMSMS', 'Domain ' . $this->get_domain() . ' updated');
    }

    /**
     * Save this theme
     * This method will send the AddDesignPre and AddDesignPost events before and after saving a new theme
     * and the EditDesignPre and EditDesignPost events before and after saving an existing theme.
     */
    public function save() {
        if ($this->get_id()) {
//            HookManager::do_hook('Core::EditDesignPre', [get_class($this) => &$this]);
            $this->_update();
//            HookManager::do_hook('Core::EditDesignPost', [get_class($this) => &$this]);
            return;
        }
//        HookManager::do_hook('Core::AddDesignPre', [get_class($this) => &$this]);
//        $this->_insert();
//        HookManager::do_hook('Core::AddDesignPost', [get_class($this) => &$this]);
    }

    /**
     * Delete the current theme
     * This class will not allow deleting themes that still have templates associated with them.
     *
     * @throws CmsLogicException
     * @param bool $force Force deleting the theme even if there are templates attached
     */
    public function delete($force = FALSE) {
        if (!$this->get_id())
            return;
        
        $db = CmsApp::get_instance()->GetDb();
        $query = 'UPDATE ' . CMS_DB_PREFIX . self::TABLENAME . ' SET active = 0 WHERE id = ?';
        $dbr = $db->Execute($query, array($this->get_id()));
        if (!$dbr)
            throw new CmsSQLErrorException($db->sql . ' -- ' . $db->ErrorMsg());

        audit($this->get_id(), 'CMSMS', 'Domain ' . $this->get_domain() . ' deleted');
        unset($this->_data['id']);
        $this->_dirty = TRUE;
    }

    /**
     * @ignore
     */
    protected static function &_load_from_data($row) {
        $ob = new CmsDomainCollection;

        $ob->_data = $row;

        return $ob;
    }

    /**
     * Load a theme object
     *
     * @throws CmsDataNotFoundException
     * @param mixed $x - Accepts either an integer theme id, or a theme name,
     * @return CmsDomainCollection
     */
    public static function &load($x) {

        $db = CmsApp::get_instance()->GetDb();
        $row = null;
        if (is_numeric($x) && $x > 0) {
            if (is_array(self::$_raw_cache) && count(self::$_raw_cache)) {
                if (isset(self::$_raw_cache[$x]))
                    return self::_load_from_data(self::$_raw_cache[$x]);
            }
            $query = 'SELECT * FROM ' . CMS_DB_PREFIX . self::TABLENAME . ' WHERE id = ?';
            $row = $db->GetRow($query, array((int) $x));
        }
        else if (is_string($x) && strlen($x) > 0) {
            if (is_array(self::$_raw_cache) && count(self::$_raw_cache)) {
                foreach (self::$_raw_cache as $row) {
                    if ($row['domain'] == $x)
                        return self::_load_from_data($row);
                }
            }

            $query = 'SELECT * FROM ' . CMS_DB_PREFIX . self::TABLENAME . ' WHERE domain = ?';
            $row = $db->GetRow($query, array(trim($x)));
        }

        if (!is_array($row) || count($row) == 0)
            throw new CmsDataNotFoundException('Could not find domain row identified by ' . $x);

        self::$_raw_cache[$row['id']] = $row;
        return self::_load_from_data($row);
    }

    /**
     * Load all domains
     *
     * @return array Array of CmsDomainCollection objects.
     */
    public static function get_all($quick = FALSE) {
        $out = null;
        $query = 'SELECT * FROM ' . CMS_DB_PREFIX . self::TABLENAME . ' WHERE active = 1 ORDER BY id ASC';
        $db = CmsApp::get_instance()->GetDb();
        $dbr = $db->GetArray($query);
        if (is_array($dbr) && count($dbr)) {
            $ids = array();
            $cache = array();
            foreach ($dbr as $row) {
                $ids[] = $row['id'];
                $cache[$row['id']] = $row;
            }
            self::$_raw_cache = $cache;
            $out = array();
            foreach ($cache as $key => $row) {
                $out[] = self::_load_from_data($row);
            }
            return $out;
        }
    }

    /**
     * Get a list of domains
     *
     * @param array Array of domains
     */
    public static function get_list() {
        $designs = self::get_all(TRUE);
        if (is_array($designs) && count($designs)) {
            $out = array();
            foreach ($designs as $one) {
                $out[$one->get_id()] = $one->get_name();
            }
            return $out;
        }
    }

    /**
     * Load the default theme
     *
     * @throws CmsInvalidDataException
     * @return CmsDomainCollection
     */
    public static function &load_default() {
        $tmp = null;
        if (self::$_dflt_id == '') {
            $query = 'SELECT id FROM ' . CMS_DB_PREFIX . self::TABLENAME . ' WHERE dflt = 1';
            $db = CmsApp::get_instance()->GetDb();
            $tmp = (int) $db->GetOne($query);
            if ($tmp > 0)
                self::$_dflt_id = $tmp;
        }

        if (self::$_dflt_id > 0)
            return self::load(self::$_dflt_id);

        throw new CmsInvalidDataException('There is no default design selected');
    }

    /**
     * Given a base name, suggest a name for a copied theme
     *
     * @param string $newname
     * @return string
     */
    public static function suggest_name($newname = '') {
        if ($newname == '')
            $newname = 'New Design';
        $list = self::get_list();
        $names = array_values($list);

        $origname = $newname;
        $n = 1;
        while ($n < 100 && in_array($newname, $names)) {
            $n++;
            $newname = $origname . ' ' . $n;
        }

        if ($n == 100)
            return;
        return $newname;
    }

}

// end of class
// dunno if this should go here...
class_alias('CmsDomainCollection', 'CmsDomainDesign');
